<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="products")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    protected UuidInterface $id;
    /**
     * @ORM\Column(type="string")
     */
    protected string $name;

    /**
     * @ORM\Column(type="boolean", options={"default": "0"})
     */
    protected bool $isUsed = false;

    public function __construct(UuidInterface $id, string $name, bool $isUsed)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isUsed = $isUsed;
    }

    public function use()
    {
        $this->isUsed = true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
