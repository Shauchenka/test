<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_prize")
 */
class UserPrize
{
    public const STATUS_PENDING = 0;
    public const STATUS_CONFIRMED = 1;
    public const STATUS_DECLINED = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    protected UuidInterface $id;
    /**
     * @ORM\Column(type="uuid")
     */
    protected UuidInterface $userId;
    /**
     * @ORM\Column(type="uuid")
     */
    protected UuidInterface $prizeId;
    /**
     * @ORM\Column(type="integer")
     */
    protected int $prizeType;
    /**
     * @ORM\Column(type="string")
     */
    protected string $value;
    /**
     * @ORM\Column(type="integer")
     */
    protected int $status = self::STATUS_PENDING;

    public function __construct(
        UuidInterface $id,
        UuidInterface $userId,
        UuidInterface $prizeId,
        int $prizeType,
        string $value
    ) {
        $this->id = $id;
        $this->userId = $userId;
        $this->prizeId = $prizeId;
        $this->prizeType = $prizeType;
        $this->value = $value;
    }

    public function confirm(): void
    {
        $this->status = self::STATUS_CONFIRMED;
    }


    public function decline(): void
    {
        $this->status = self::STATUS_DECLINED;
    }

    public function getPrizeType(): int
    {
        return $this->prizeType;
    }

    public function isPending(): bool
    {
        return $this->status === self::STATUS_PENDING;
    }

    /**
     * @return UuidInterface
     */
    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    /**
     * @return UuidInterface
     */
    public function getPrizeId(): UuidInterface
    {
        return $this->prizeId;
    }


}