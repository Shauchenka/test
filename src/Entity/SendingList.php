<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sending_list")
 */
class SendingList
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    protected UuidInterface $id;
    /**
     * @ORM\Column(type="string")
     */
    protected string $userId;
    /**
     * @ORM\Column(type="string")
     */
    protected string $productId;
    /**
     * @ORM\Column(type="boolean")
     */
    protected bool $sent = false;

    public function __construct(string $userId, string $productId)
    {
        $this->id = Uuid::uuid4();
        $this->userId = $userId;
        $this->productId = $productId;
    }
}