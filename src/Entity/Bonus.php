<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="bonus")
 */
class Bonus
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    protected UuidInterface $id;
    /**
     * @ORM\Column(type="integer")
     */
    protected int $sum;

    public function __construct(UuidInterface $id, int $sum)
    {
        $this->id = $id;
        $this->sum = $sum;
    }
}
