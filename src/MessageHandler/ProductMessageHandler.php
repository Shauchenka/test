<?php

namespace App\MessageHandler;

use App\Entity\SendingList;
use App\Entity\TestResult;
use App\Message\MessageInterface;
use Doctrine\ORM\EntityManagerInterface;

class ProductMessageHandler implements QueueHandlerInterface
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function handle(MessageInterface $message)
    {
        $result = new SendingList($message->getUserId(), $message->getProductId());
        $this->em->persist($result);
        $this->em->flush();
    }
}