<?php


namespace App\MessageHandler;


use App\Message\MessageInterface;

interface QueueHandlerInterface
{
    public function handle(MessageInterface $message);
}
