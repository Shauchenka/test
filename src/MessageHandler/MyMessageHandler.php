<?php

namespace App\MessageHandler;

use App\Entity\TestResult;
use App\Message\MessageInterface;
use Doctrine\ORM\EntityManagerInterface;

class MyMessageHandler implements QueueHandlerInterface
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function handle(MessageInterface $message)
    {
        $result = new TestResult($message->getName());
        $this->em->persist($result);
        $this->em->flush();
    }
}