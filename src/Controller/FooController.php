<?php

namespace App\Controller;

use App\Message\BonusMessage;
use App\Service\Container;
use App\Service\PrizeService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FooController
{

    public function indexAction(): JsonResponse
    {
        /** @var EntityManagerInterface $em */
        $em = Container::get(EntityManager::class);
        $prizeService = new PrizeService($em);
        $prize = $prizeService->getPrize();

        return new JsonResponse($prize);
    }

    public function confirmAction(string $id): JsonResponse
    {
        if(Uuid::isValid($id) === false) {
            throw new BadRequestHttpException('invalid id. Only UUID');
        }

        /** @var EntityManagerInterface $em */
        $em = Container::get(EntityManager::class);
        $prizeService = new PrizeService($em);
        $prizeService->confirm($id);

        return new JsonResponse();
    }
}