<?php

namespace App\Service;

class ProductRedisService
{
    private static ?\Redis $redis = null;
    private const REDIS_KEY = 'product:';

    public function __construct()
    {
        if (is_null(self::$redis)) {
            self::$redis = new \Redis();
            self::$redis->connect($_ENV['REDIS_HOST']);
            self::$redis->auth($_ENV['REDIS_SECRET']);
        }
    }

    public function getRandomAndRemove(): ?string
    {
        $key = self::$redis->randomKey();

        self::$redis->multi();
        self::$redis->get($key);
        self::$redis->del($key);
        $value = self::$redis->exec();

        return isset($value[0]) ? $value[0] : null;
    }

    public function add(string $id): void
    {
        self::$redis->set(self::REDIS_KEY . $id, $id);
    }

    public function remove(string $id): void
    {
        self::$redis->del(self::REDIS_KEY . $id);
    }

    public function clearAll(): void
    {
        self::$redis->flushDB();
    }
}
