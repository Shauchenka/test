<?php

namespace App\Service\Selector;

use App\DTO\PrizeDTO;
use App\Entity\Bonus;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;

class BonusSelector implements SelectorInterface
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getPrize(): PrizeDTO
    {
        $id = Uuid::uuid4();
        $sum = rand($_ENV['BONUS_MIN'], $_ENV['BONUS_MAX']);
        $money = new Bonus($id, $sum);
        $this->em->persist($money);
        $this->em->flush();

        return new PrizeDTO($id, $sum, PrizeDTO::TYPE_PRODUCT);
    }
}