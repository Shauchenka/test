<?php

namespace App\Service\Selector;

use App\DTO\PrizeDTO;

interface SelectorInterface
{
    public function getPrize(): PrizeDTO;
}