<?php

namespace App\Service\Selector;

use App\DTO\PrizeDTO;
use App\Service\Selector\SelectorInterface;
use Doctrine\ORM\EntityManagerInterface;

class SelectorFactory
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getSelector(int $type): SelectorInterface
    {
        return new ProductSelector($this->em);
        switch ($type) {
            case PrizeDTO::TYPE_BONUS:
                return new BonusSelector($this->em);
            case PrizeDTO::TYPE_MONEY:
                return new MoneySelector($this->em);
            case PrizeDTO::TYPE_PRODUCT:
            default:
                return new ProductSelector($this->em);
        }
    }
}