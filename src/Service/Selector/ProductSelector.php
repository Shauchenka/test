<?php

namespace App\Service\Selector;

use App\DTO\PrizeDTO;
use App\Entity\Product;
use App\Service\ProductRedisService;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;

class ProductSelector implements SelectorInterface
{
    private EntityManagerInterface $em;
    private ProductRedisService $productRedisService;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->productRedisService = new ProductRedisService();
    }

    public function getPrize(): PrizeDTO
    {
        $id = $this->productRedisService->getRandomAndRemove();

        $product = $this->em->getRepository(Product::class)->find(Uuid::fromString($id));

        $product->use();
        $this->em->flush();

        return new PrizeDTO(
            $id,
            $product->getName(),
            PrizeDTO::TYPE_PRODUCT
        );
    }
}