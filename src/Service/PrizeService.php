<?php

namespace App\Service;

use App\DTO\UserPrizeDTO;
use App\Entity\UserPrize;
use App\Message\PrizeMessageBuilder;
use App\Service\Selector\SelectorFactory;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Rfc4122\UuidV4;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PrizeService
{
    private EntityManagerInterface $em;
    private SelectorFactory $selectorFactory;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->selectorFactory = new SelectorFactory($this->em);
    }

    public function getPrize(): UserPrizeDTO
    {
        $id = Uuid::uuid4();
        $userId = Uuid::uuid4(); //TODO: change to random user

        $selector = $this->selectorFactory->getSelector(rand(0, 2));
        $prize = $selector->getPrize();

        $userPrize = new UserPrize(
            $id,
            $userId,
            UuidV4::fromString($prize->id),
            $prize->type,
            $prize->value
        );

        $this->em->persist($userPrize);
        $this->em->flush();

        return new UserPrizeDTO($id, $prize->value, $prize->type);
    }

    public function confirm(string $id): void
    {
        $prize = $this->em->getRepository(UserPrize::class)->find(Uuid::fromString($id));

        if ($prize->isPending() === false) {
            throw new \LogicException('Prize already confirmed');
        }

        if (is_null($prize)) {
            throw new BadRequestHttpException('Prize not found');
        }

        $prize->confirm();
        $this->em->flush();

        $queueService = new QueueService($prize->getPrizeType());
        $message = PrizeMessageBuilder::build($prize->getPrizeType(), $prize->getUserId(), $prize->getPrizeId());

        $queueService->send($message);
    }
}