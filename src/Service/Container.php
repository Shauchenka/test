<?php

namespace App\Service;

class Container
{
    private static array $storage = [];

    public static function add(object $object)
    {
        self::$storage[get_class($object)] = $object;
    }

    public static function get(string $className): ?object
    {
        return self::$storage[$className] ?? null;
    }
}