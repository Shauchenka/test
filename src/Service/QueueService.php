<?php

namespace App\Service;

use App\DTO\PrizeDTO;
use App\Message\MessageInterface;
use App\MessageHandler\BonusMessageHandler;
use App\MessageHandler\MoneyMessageHandler;
use App\MessageHandler\ProductMessageHandler;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpReceiver;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpTransport;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\Connection;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface;
use Symfony\Component\Messenger\Transport\Serialization\Serializer;
use Symfony\Component\Messenger\Transport\TransportInterface;

class QueueService
{
    public const PRODUCT_QUEUE = 'product';
    public const BONUS_QUEUE = 'bonus';
    public const MONEY_QUEUE = 'money';

    public const QUEUE_MAP = [
        self::PRODUCT_QUEUE => ProductMessageHandler::class,
        self::BONUS_QUEUE => BonusMessageHandler::class,
        self::MONEY_QUEUE => MoneyMessageHandler::class,
    ];

    public const QUEUE_TYPE_MAP = [
        PrizeDTO::TYPE_PRODUCT => self::PRODUCT_QUEUE,
        PrizeDTO::TYPE_BONUS => self::BONUS_QUEUE,
        PrizeDTO::TYPE_MONEY => self::MONEY_QUEUE,
    ];

    private ReceiverInterface $receiver;
    private TransportInterface $transport;

    public function __construct(int $type)
    {
        $serializer = new Serializer();
        $queue = $this->getQueueByType(2);

        $this->transport = new AmqpTransport(
            Connection::fromDsn($_ENV['RABBIT_AMQP'] . $queue, ['host' => 'task-rabbitmq']), $serializer
        );

        $this->receiver = new AmqpReceiver(
            Connection::fromDsn(
                $_ENV['RABBIT_AMQP'] . $queue,
                ['host' => 'task-rabbitmq']
            ),
            $serializer
        );
    }

    public function get(): ?MessageInterface
    {
        $actualEnvelopes = iterator_to_array($this->receiver->get());
        $message = null;

        /** @var Envelope $actualEnvelope */
        foreach ($actualEnvelopes as $actualEnvelope) {
            /** @var MessageInterface $message */
            $message = $actualEnvelope->getMessage();
            $this->receiver->ack($actualEnvelope);
        }

        return $message;
    }

    public function send(MessageInterface $message)
    {
        $this->transport->send(new Envelope($message));
    }

    public function count(): int
    {
        return $this->receiver->getMessageCount();
    }

    public function getHandlerName(int $type): ?string
    {
        $queue = $this->getQueueByType($type);

        return isset(self::QUEUE_MAP[$queue]) ? self::QUEUE_MAP[$queue] : null;
    }

    private function getQueueByType(int $type): string
    {
        $queue = self::QUEUE_TYPE_MAP[$type] ?? null;

        if (is_null($queue)) {
            throw new \LogicException('Unsupported type');
        }

        return $queue;
    }
}