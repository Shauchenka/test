<?php

namespace App\DTO;

class PrizeDTO
{
    public const TYPE_MONEY = 0;
    public const TYPE_BONUS = 1;
    public const TYPE_PRODUCT = 2;

    public const TYPES_LIST = [
        self::TYPE_MONEY,
        self::TYPE_BONUS,
        self::TYPE_PRODUCT
    ];

    public string $id;
    public string $value;
    public int $type;

    public function __construct(string $id, string $value, int $type)
    {
        $this->id = $id;
        $this->value = $value;
        $this->type = $type;
    }
}
