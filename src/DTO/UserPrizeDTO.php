<?php


namespace App\DTO;


class UserPrizeDTO
{
    public string $id;
    public string $value;
    public int $type;

    public function __construct(string $id, string $value, int $type)
    {
        $this->id = $id;
        $this->value = $value;
        $this->type = $type;
    }
}