<?php

namespace App\Command;

namespace App\Command;

use App\MessageHandler\QueueHandlerInterface;
use App\Service\QueueService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConsumeCommand extends Command
{
    /** @var EntityManagerInterface */
    private EntityManagerInterface $em;

    protected static $defaultName = 'app:consume';
    private QueueService $queueService;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->em = $em;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $printWait = false;
        $this->queueService = new QueueService(2);
        while (true) {
            if ($this->queueService->count() === 0) {
                if ($printWait === false) {
                    $output->writeln('wait...');
                    sleep(5);
                    $printWait = true;
                }
            } else {
                $message = $this->queueService->get();
                $handlerName = $this->queueService->getHandlerName($message->getType());

                if(is_null($handlerName)) {
                    continue;
                }

                /** @var QueueHandlerInterface $handler */
                $handler = new $handlerName($this->em);
                $handler->handle($message);
                $output->writeln('Processed!');
                $printWait = false;
            }
        }
    }
}