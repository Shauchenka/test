<?php

namespace App\Command;

namespace App\Command;

use App\Entity\Product;
use App\Entity\User;
use App\Service\ProductRedisService;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestDataCommand extends Command
{
    /** @var EntityManagerInterface */
    private EntityManagerInterface $em;
    private ProductRedisService $redisService;

    protected static $defaultName = 'app:test-command';

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->redisService = new ProductRedisService();
        $this->em = $em;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->truncate();
        $this->redisService->clearAll();
        $this->makeUsers();
        $this->makeProducts();

        $this->em->flush();

        return Command::SUCCESS;
    }

    private function makeProducts(): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $id = Uuid::uuid4();

            $product = new Product(
                $id,
                $faker->word(),
                $faker->boolean
            );

            $this->em->persist($product);
            $this->redisService->add($id->toString());
        }
    }

    private function makeUsers(): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $id = Uuid::uuid4();

            $product = new User(
                $id,
                $faker->name()
            );

            $this->em->persist($product);
        }
    }

    protected function truncate(): void
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

        $this->em->getConnection()->prepare("SET FOREIGN_KEY_CHECKS = 0;")->execute();

        foreach ($this->em->getConnection()->getSchemaManager()->listTableNames() as $tableNames) {
            $sql = 'TRUNCATE TABLE ' . $tableNames;
            $this->em->getConnection()->prepare($sql)->execute();
        }
        $this->em->getConnection()->prepare("SET FOREIGN_KEY_CHECKS = 1;")->execute();
    }

}