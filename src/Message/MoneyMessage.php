<?php

namespace App\Message;

use App\DTO\PrizeDTO;

class MoneyMessage extends PrizeMessage
{
    protected const TYPE = PrizeDTO::TYPE_MONEY;
}