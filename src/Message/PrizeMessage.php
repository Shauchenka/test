<?php

namespace App\Message;

use App\DTO\PrizeDTO;

class PrizeMessage implements MessageInterface
{
    protected const TYPE = -1;

    private string $userId;
    private string $productId;

    public function __construct(string $userId, string $productId)
    {
        $this->userId = $userId;
        $this->productId = $productId;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getType(): int
    {
        return static::TYPE;
    }
}