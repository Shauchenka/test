<?php

namespace App\Message;

use App\DTO\PrizeDTO;

class ProductMessage extends PrizeMessage
{
    protected const TYPE = PrizeDTO::TYPE_PRODUCT;
}