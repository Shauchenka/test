<?php


namespace App\Message;


use App\DTO\PrizeDTO;

class PrizeMessageBuilder
{
    public static function build(int $type, string $userId, string $productId): PrizeMessage
    {
        switch ($type) {
            case PrizeDTO::TYPE_BONUS:
                return new BonusMessage($userId, $productId);
            case PrizeDTO::TYPE_MONEY:
                return new MoneyMessage($userId, $productId);
            case PrizeDTO::TYPE_PRODUCT:
                return new ProductMessage($userId, $productId);
            default:
                throw new \LogicException('Unsupported type');
        }
    }
}