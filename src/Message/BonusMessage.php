<?php


namespace App\Message;


use App\DTO\PrizeDTO;

class BonusMessage extends PrizeMessage
{
    protected const TYPE = PrizeDTO::TYPE_BONUS;
}