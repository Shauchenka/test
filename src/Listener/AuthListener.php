<?php

namespace App\Listener;

use App\Event\PreRouteEvent;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class AuthListener implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{

    public function onPreRoute(PreRouteEvent $event)
    {
        echo 'event is worked';
        throw new AuthenticationException('please set login and pass in session');
    }

    public static function getSubscribedEvents()
    {
        return [
            PreRouteEvent::NAME => 'onPreRoute'
        ];
    }
}