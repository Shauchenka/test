<?php
require_once dirname(__FILE__) . "/../vendor/autoload.php";

use App\Service\Container;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Ramsey\Uuid\Doctrine\UuidType;
use Symfony\Component\Dotenv\Dotenv;

$isDevMode = true;
$proxyDir = null;
$cache = null;
$useSimpleAnnotationReader = false;
$config = Setup::createAnnotationMetadataConfiguration(array("src/Entity"), $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/../.env');

$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => $_ENV['DB_USER'],
    'password' => $_ENV['DB_PASS'],
    'dbname'   => $_ENV['DB_NAME'],
    'host'     => $_ENV['DB_HOST'],
    'port'     => $_ENV['DB_PORT'],
);

// obtaining the entity manager
try {
    $entityManager = EntityManager::create($dbParams, $config);
    Type::addType('uuid', UuidType::class);
    $entityManager->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('db_uuid', 'uuid');
    Container::add($entityManager);
} catch (ORMException $e) {
echo 'fail';
die;
}