<?php

use App\Event\PreRouteEvent;
use App\Listener\AuthListener;
use Symfony\Component\EventDispatcher\EventDispatcher;
require_once "../public/bootstrap.php";
$dispatcher = new EventDispatcher();
$dispatcher->addListener(PreRouteEvent::NAME, [ new AuthListener(), 'onPreRoute']);
