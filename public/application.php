<?php

require_once "public/bootstrap.php";

use App\Command\ConsumeCommand;
use App\Command\TestDataCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new TestDataCommand($entityManager));
$application->add(new ConsumeCommand($entityManager));

$application->run();