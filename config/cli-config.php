<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

require_once "public/bootstrap.php";

return ConsoleRunner::createHelperSet($entityManager);

